var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'eu-west-1'});

// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */
exports.postDadJoke = async (event, context) => {
    try {
        // const ret = await axios(url);
        // Create the DynamoDB service object
        var ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

        //sam-app-DynamoJokeTable-12NPP6B8YE8TF

        var params = {
        TableName: 'JOKE_TABLE',
        Item: {
            'JOKE_ID' : {N: '001'},
            'JOKE_START' : {S: 'Which sea creature makes the best tuba player?'},
            'JOKE_END' : {S: 'The blow fish.'},
        }
        };

        // Call DynamoDB to add the item to the table
        ddb.putItem(params, function(err, data) {
            if (err) {
                console.log("Error", err);
            } else {
                console.log("Success", data);
            }

            response = {
                'statusCode': 200,
                'body': JSON.stringify({
                    message: 'OK',
                    err,
                    data
                    // location: ret.data.trim()
                })
            }

            return response
        });
        
    } catch (err) {
        console.log(err);
        return err;
    }

    
};
